from data_time import checking_overtime
day = input(str("What day is it? "))
try:
    toa = int(input("What is the time of arrival? (Excluding Minutes) "))
    if toa < 8 or toa > 23:
        print("Parking is only allowed 8 AM - midnight")
        exit()
    hours = int(input("How many hours parked? "))
except ValueError:
    print("This input has to be numbers")
    exit()
pn = input("What is your parking number? (Enter 4 digits) ")
if len(pn) != 4:
    print("Error: Parking number must be a 4-digit number")
    exit()
pn = int(pn)

Monday = 8
Tuesday = 8
Wednesday = 8
Thursday = 8
Friday = 8
Saturday = 1
Sunday = 0
ped = 2
daylist = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
fpn = [1234, 5678, 1111, 2222, 3333]
if day in daylist:
    if 8 <= int(toa) < 16:
        result_overtime = checking_overtime(day)
        if(result_overtime):
            print("The maximum limit of hours is ", 24 - toa)
            exit()
    if 16 <= int(toa) < 24:
        if day == "Monday":
            day = 0
            if hours > 24 - toa:
                print("The maximum limit of hours is ", 24 - toa)
                exit()
        if day == "Tuesday":
            day = 0
            if hours > 24 - toa:
                print("The maximum limit of hours is ", 24 - toa)
                exit()
        if day == "Wednesday":
            day = 0
            if hours > 24 - toa:
                print("The maximum limit of hours is ", 24 - toa)
                exit()
        if day == "Thursday":
            day = 0
            if hours > 24 - toa:
                print("The maximum limit of hours is ", 24 - toa)
                exit()
        if day == "Friday":
            day = 0
            if hours > 24 - toa:
                print("The maximum limit of hours is ", 24 - toa)
                exit()
        if day == "Saturday":
            day = 0
            if hours > 24 - toa:
                print("The maximum limit of hours is ", 24 - toa)
                exit()
        if day == "Sunday":
            day = 0
            if hours > 24 - toa:
                print("The maximum limit of hours is ", 24 - toa)
                exit()
else:
        print("The day was incorrect, please try again.")
        exit()
price = (ped + day) * hours
discount = 1
if pn in fpn:
    if 8 <= int(toa) < 16:
        discount = 0.9
    if 16 <= int(toa) < 24:
        discount = 0.5
print(price * discount)